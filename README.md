# shimansky.herokuapp.com

Portfolio of Serguei Shimansky better known as englishextra

[![shimansky.herokuapp.com](http://farm8.staticflickr.com/7111/27917988781_7213a201fa_o.jpg)](https://shimansky.herokuapp.com/)

## On-line

 - [the website](https://shimansky.herokuapp.com/)
 
## Dashboard

<https://dashboard.heroku.com/apps/shimansky>
 
## Production Push URL

```
https://git.heroku.com/shimansky.git
```

## Remotes

 - [GitHub](https://github.com/englishextra/shimansky.herokuapp.com)
 - [BitBucket](https://bitbucket.org/englishextra/shimansky.herokuapp.com)
 - [GitLab](https://gitlab.com/englishextra/shimansky.herokuapp.com)
